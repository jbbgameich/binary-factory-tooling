// Request a node to be allocated to us
node( "AndroidARMSDK" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// Android application builds are performed by a single script, so let's get that underway
		stage('Building Application') {
			// Do the build!
			sh """
				${dependency}
				export EXTRA=-DKDE_L10N_AUTO_TRANSLATIONS=ON
				/opt/helpers/build-generic ${application} ${cmakeParameters}

				for apk in \$(echo build/*/*_build_apk/build/outputs/apk/*/*.apk); do
					mv \$apk $ANDROID_ARCH-\$(basename \$apk)
				done
			"""

			// Stash the APKs we now have for later...
			stash includes: '*.apk', name: 'arm-apks'
		}
	}
}
}

// Request a node to be allocated to us
node( "AndroidAArch64SDK" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// Android application builds are performed by a single script, so let's get that underway
		stage('Building Application') {
			// Do the build!
			sh """
				${dependency}
				export EXTRA=-DKDE_L10N_AUTO_TRANSLATIONS=ON
				/opt/helpers/build-generic ${application} ${cmakeParameters}

				for apk in \$(echo build/*/*_build_apk/build/outputs/apk/*/*.apk); do
					mv \$apk $ANDROID_ARCH-\$(basename \$apk)
				done
			"""

			// Stash the APKs we now have for later...
			stash includes: '*.apk', name: 'aarch64-apks'
		}
	}
}
}

// With the APKs all built, we now need to transfer over to the signing machine as plain APKs won't be of much use to people
// We should also use this opportunity to make the APKs available in our F-Droid repository
node( "AndroidSigner" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {
		// Time to sign!
		stage('Signing APKs') {
			// Make sure we have a clean environment first
			deleteDir()

			// Retrieve the APKs we captured earlier
			unstash 'arm-apks'
			unstash 'aarch64-apks'

			// Perform the signing process
			// By default we use the Keystore and associated details from ~/keys/android-signing-config
			// However if there is a package specific configuration, then we will use that instead
			// All APK files will be subject to zipalign prior to being signed in accordance with the Google documentation on signing of APKs
			sh """
				bash ~/binary-factory-tooling/android/perform-signing.sh $application
			"""
		}

		// Capture the APKs to make them downloadable through Jenkins for those who prefer to sideload the APK files
		stage('Capturing APKs') {
			// Now grab the APKs it generated
			archiveArtifacts artifacts: '*.apk', onlyIfSuccessful: true
		}

		// Finally, publish them to our F-Droid repository
		stage('Publishing Repository') {
			// We'll move the files into the repository now, and then invoke the script designated to publish these artifacts
			sh """
				export PATH=$HOME/.local/bin/:$HOME/bin/:$PATH

				mv *.apk ~/repository/repo/
				python3 ~/binary-factory-tooling/android/generaterepo.py --fdroid-repository ~/repository/
			"""
		}
	}
}
}
